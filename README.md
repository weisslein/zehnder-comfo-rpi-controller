# zehnder-comfo-rpi-controller

## Overview
Web of Things (WoT) based project, that gives the possibility to control a Zehnder Comfo ventilation system  additionally over a Raspberry PI (RPI).  project should work with similar systems like Comfod350, Comfoair350, WHR930 or StorkAir - no guarantee!

##  Hardware - Ventilation system integration with RPI
The Zehnder Comfo ventilation system offers the following opportunities for an integration with an homeautomation system:
- using the internal RS232 port
- replace the manual rotary switch with an Raspberry PI 

If you are still in the warranty phase, the first option is not recommanded. Zehnder itself recommanded me to use option 2. this projects describes the extension of the manual rotary switch with an RPI to control the ventilation system per Mobile App and/or a home automation system.

## RPI as Things Gateway
### Hardware artefacts
- Raspberry PI (I've used an RPI 3)
- at least 3 highly loadable relays with an switching capacity of AC: max. 250V / 10A 
- power supply for the RPI

### Hardware integration
The manufacturer Zehnder recommanded me to connect the ventilation system with 3 relays according to the following specification:
![manufacturer circuit diagram](images/connect_rotaryswitch_relay_ventilation_system.png?raw=true "manufacturer circuit diagram")

the standard rotary switch has the following PIN assignment:
- Level 1 - grey cable/wire
- Level 2 - black cable/wire
- Level 3 - brown cable/wire

The operation level of the ventilation system are defined as follows:
- Level 1 - current on L3 means Relay 1 must be closed
- Level 2 - current on L1 + L2 means Relay 1 and 2 closed
- Level 3 - current on L1 + L2 + L3 means all 3 relays closed (1+2+3)

The circuit diagram together with the RPI and the relays looks as follows:
![wiring setup](images/fritzing_comfoD_turn_switch_with_rpi_steckplatine.png?raw=true "wiring setup")

### RPI Installation & Configuration
Just install your RPI as stated in the RPI installation guide. I've used NOOBS for the installation.
Further documenation can be found under:
https://www.raspberrypi.org/downloads/noobs/

Attention: After the installation  change your pi password.

Please install the following components on your RPI:
- Node version 4.x (to avoid the WoT Object.observe Bug (Issue #3) please use Node version 4.x.!) [further information](https://github.com/webofthings/webofthings.js/issues/3)
  - Please install the correct node version for your RPI use 
    ```
    uname -m
    ```
    to detect the ARM version you have.
- Zehnder WoT Gateway software components
  - download the software artefacts
    ```
    git clone https://gitlab.com/weisslein/zehnder-comfo-rpi-controller
    ```
  - install the required npm modules

    go into the downloaded git folder and run
    ```
    sudo npm install
    ```
    Note: please run npm with sudo! the Hardware related modules (eg. onoff, etc) requires these permissions!

  - enabling https and wss with tls

    create the certifcates within the resource folder of the nodejs app:
    ```console
    openssl req -sha256 -newkey rsa:2048 -keyout privateKey.pem -out caCert.pem -days 10950 -x509
    ```
    During the generation a prompt comes ng passphrase within the wot-server.js in line 18.

  - access control with REST and API token

    Uncommenting the following line in the servers/http.js file:

    ```javascript
    console.info('Here is a new random crypto-secure API Key: ' + utils.generateApiToken());
    ```
    - Launch the node server wot.js
    - Copy and paste the value of apiToken in the resource/auth.json file
    - Note: this will be the API token to send any request to the wot gateway 
    - After the generation of the API token comment the line within the server/http.js

  - configure your ventilation system

    adapt the model (resource/jsonLD.json) according to your real ventilation system product, which means adjust the following lines:
    ```javascript
    ...
    "productID": "Serialno:000795767707",
    "manufacturer": "Zehnder",
    "model": "ComfoD 350 L",
    ...
    ```

### Web of Things interfaces

- semantic interfaces
  the gateway offers a human readable description in HTML and a machine readable description in JSON as well as JSON-LD.
  - HTML interface

  call https://<yourgatewayhost>:8484 within your browser. then you can navigate through the Web of Things representation of the ventilation system.

  - JSON interface
  returns the semantical representation of the thing as json without reference to any standards.

  the following Listing return the properties of the thing:
  ```bash
  curl -i -H "Accept: application/json" -H "token: <your token>" -X GET 'http://<yourhostname>:8484/properties'
  ``` 

  - JSON-LD interface
  returns the semantical representation of the thing based on the the WOT Standard (JSON-LD)

  The listing below returns e.g. all product related data:
  ```bash
  curl -i -H "Accept: application/json+ld" -H "token: <your token>" -X GET 'http://<your hostname>s:8484/'
  ``` 

- acumulators interface

  To operate the ventilation system on the 3 levels, please send on of the following POST requests to the gateway:
  
  1) Level 0 - ventilation system switched off

    ```bash
  curl -i -H "Content-Type: application/json" -H "Accept: application/json" -H "token: <your token>" -X POST 'http://<yourhostname>:8484/actions/levelState' -d '{"levelId":"0"}'
    ```
  2) Level 1 - 1st ventilation fan level - 30% circulation

    ```bash
  curl -i -H "Content-Type: application/json" -H "Accept: application/json" -H "token: <your token>" -X POST 'http://<yourhostname>:8484/actions/levelState' -d '{"levelId":"1"}'
    ```
  3) Level 2 - 2nd ventilation fan level - 50% circulation

    ```bash
  curl -i -H "Content-Type: application/json" -H "Accept: application/json" -H "token: <your token>" -X POST 'http://<yourhostname>:8484/actions/levelState' -d '{"levelId":"2"}'
    ```
  4) Level 3 - 3rd ventilation fan level - full speed

    ```bash
  curl -i -H "Content-Type: application/json" -H "Accept: application/json" -H "token: <your token>" -X POST 'http://<yourhostname>:8484/actions/levelState' -d '{"levelId":"3"}'
    ```
## Openhab Integration

### V1 integration - http binding based
Note: the http binding is a "hard" coupling between your WoT thing and the homeautomation system. A more sophisticated approach with an automatical finding and instantiating process is planned for the future. (see below)
- generate an item with an http binding within Openhab

This item represents the current operation mode of the ventilation system within Openhab. 
```
Number ventilation_system_operation_mode "Belüftungsanlage" <fan>  { http="<[http://<hostname>:8484/properties{Accept=%22application/json%22}:5000:JSONPATH($[0].values.operationMode)]" }
```
- The latest http binding doesn't support JSON with POST request, therefore to change the operation level of the ventilation system a rule must be used instead. The rule will be triggered after the item was changed within the Openhab UI and sends then an JSON POST request to the WoT gateway.
```
when
	Item ventilation_system_operation_mode changed 
then
	logInfo("Item ventilation_system_operation_mode changed to "+ventilation_system_operation_mode.state)
	val SRV_URL = "http://<hostnameOfWoTGateway>:8484/actions/levelState?token=<token>"

	var contenttype = "application/json"
	var request = ' '
	 if(ventilation_system_operation_mode.state == 0) {
			request = '{"levelId": "0"}'
	 }
	 if(ventilation_system_operation_mode.state == 1) {
			request = '{"levelId": "1"}'
	 }	
	 if(ventilation_system_operation_mode.state == 2) {
			request = '{"levelId": "2"}'
	 }
	 if(ventilation_system_operation_mode.state == 3) {
			request = '{"levelId": "3"}'
	 }		 
	sendHttpPostRequest(SRV_URL, contenttype, request)
	
	logInfo("PostRequest:"+request)
end
```



### V2 integration - Binding for own WoS implementation
coming soon - goal is a Openhab V2 binding that offers automatical finding/discovery of WoT based things and/or products (via mDNS) 

## open points
- Connecting Zehnder ComfoD350 via RS232  (after warranty)
- Openhab V2 binding with discovery/finding support