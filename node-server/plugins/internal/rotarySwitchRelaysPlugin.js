var CorePlugin = require('./../corePlugin').CorePlugin,
    util = require('util'),
    utils = require('./../../utils/utils.js');

var actuatorL1, actuatorL2, actuatorL3, model;

var RotarySwitchRelaysPlugin = exports.RotarySwitchRelaysPlugin = function (params) { //#A
    CorePlugin.call(this, params, 'state', stop, simulate, ['levelState'], switchLevelFunc); //#B
    model = this.model;
    this.addValue('0');
};
util.inherits(RotarySwitchRelaysPlugin, CorePlugin); //#C

function switchLevelFunc(value) {
    var self = this;
    console.info('Changed value of %s to %s', self.model.name, value.levelId);
    if (!this.params.simulate) {
        //get latest properties of the model

        console.info('Actuator L1 has value:', actuatorL1.readSync());
        console.info('Actuator L2 has value:', actuatorL2.readSync());
        console.info('Actuator L3 has value:', actuatorL3.readSync());
        if (value.levelId === "1") {
            //set operations mode L1
            actuatorL1.write(0);
            actuatorL2.write(1);
            actuatorL3.write(1);
        } else if (value.levelId === "2") {
            //set operations mode L2
            actuatorL1.write(0);
            actuatorL2.write(0);
            actuatorL3.write(1);
        } else if (value.levelId === "3") {
            //set operations mode L3
            actuatorL1.write(0);
            actuatorL2.write(0);
            actuatorL3.write(0);
        } else {
            //set operations mode L0
            actuatorL1.write(1);
            actuatorL2.write(1);
            actuatorL3.write(1);
        }
        console.info('Acutator L1 changed value to:', actuatorL1.readSync());
        console.info('Acutator L2 changed value to:', actuatorL2.readSync());
        console.info('Acutator L3 changed value to:', actuatorL3.readSync());
    }
    self.addValue(value.levelId);
    value.status = 'completed'; //#E
    console.info('Changed value of %s to %s', self.model.name, value.levelId);
};

function stop() {
    actuatorL1.unexport();
    actuatorL2.unexport();
    actuatorL3.unexport();
};

function simulate() {
    // this.addValue("1");
};

RotarySwitchRelaysPlugin.prototype.createValue = function (data) {
    if (data === "1") {
        //true means = HIGH at the specified GPIOs 
        //false means = LOW at the specified GPIOs and the relay activates then
        //set operations mode L1
        return { "operationMode": data,"L1": false, "L2": true, "L3": true, "timestamp": utils.isoTimestamp() };
    } else if (data === "2") {
        //set operations mode L1
        return { "operationMode": data, "L1": true, "L2": true, "L3": false, "timestamp": utils.isoTimestamp() };
    } else if (data === "3") {
        //set operations mode L1
        return { "operationMode": data, "L1": true, "L2": true, "L3": true, "timestamp": utils.isoTimestamp() };
    } else {
        //set operations mode L0
        return { "operationMode": '0', "L1": true, "L2": true, "L3": true, "timestamp": utils.isoTimestamp() };
    }
};

RotarySwitchRelaysPlugin.prototype.connectHardware = function () { //#F
    var Gpio = require('onoff').Gpio; //#G
    var self = this;

    console.info('Hardware %s actuator started!', self.model.name);

    actuatorL1 = new Gpio(self.model.values.L1.customFields.gpio, 'high');
    actuatorL2 = new Gpio(self.model.values.L2.customFields.gpio, 'high');
    actuatorL3 = new Gpio(self.model.values.L3.customFields.gpio, 'high');

    console.info('Actuator L1 started with value:', actuatorL1.readSync());
    console.info('Actuator L2 started with value:', actuatorL2.readSync());
    console.info('Actuator L3 started with value:', actuatorL3.readSync());
};

//#A Call the initalization function of the parent plugin (corePlugin.js)
//#B Pass it the property you’ll update (state) and the actions you want to observe (levelState) as well as the implementation of what to do when a levelState action is created (switchLevelFunc)
//#C Make the RotarySwitchRelaysPlugin inherit from all the corePlugin.js functionality
//#D Add a new data entry to the property in the model
//#E Change status to 'completed' as the rotary switch state was changed
//#F Extend the function connectHardware of corePlugin.js
//#G Change the state of the rotary switch using the on/off library

