var restApp = require('./servers/http'),
  wsServer = require('./servers/websockets'),
  resources = require('./resources/model'),
  fs = require('fs');


var createServer = function (port, secure) {
  if (process.env.PORT) port = process.env.PORT;
  else if (port === undefined) port = resources.customFields.port;
  if (secure === undefined) secure = resources.customFields.secure;

  initPlugins(); //#A

  if(secure) {
    var https = require('https'); //#B
    var certFile = './resources/caCert.pem'; //#C
    var keyFile = './resources/privateKey.pem'; //#D
    var passphrase = 'wotpassphrase'; //#E - please modify the passphrase according to your needs

    var config = {
      cert: fs.readFileSync(certFile),
      key: fs.readFileSync(keyFile),
      passphrase: passphrase
    };

    return server = https.createServer(config, restApp) //#F
      .listen(port, function () {
        wsServer.listen(server); //#G
        console.log('Secure WoT server for the ventilation system started on port %s', port);
    })
  } else {
    var http = require('http');
    return server = http.createServer(restApp)
      .listen(process.env.PORT || port, function () {
        wsServer.listen(server);
        console.log('Insecure WoT server for the ventilation system started on port %s', port);
    })
  }
};

function initPlugins() {
  var RotarySwitchRelaysPlugin = require('./plugins/internal/rotarySwitchRelaysPlugin').RotarySwitchRelaysPlugin;

  switchPlugin = new RotarySwitchRelaysPlugin({'simulate': false, 'frequency': 5000});
  switchPlugin.start();
}

module.exports = createServer;

process.on('SIGINT', function () {
  switchPlugin.stop();
  console.log('Bye, bye!');
  process.exit();
});